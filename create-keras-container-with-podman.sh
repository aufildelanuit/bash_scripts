#!/bin/bash

#podman run -u $(id -u):$(id -g) -it --name keras --pod new:keras_pod -p 127.0.0.1:8005:8000 -v /var/home/${USER}/Programming/python/keras/:/shared:z -w /shared --userns keep-id tensorflow/tensorflow:latest-py3
podman run -it --name keras --pod new:keras_pod -p 127.0.0.1:8988:8988 -v /var/home/${USER}/Programming/python/keras/:/shared:z -w /shared --userns keep-id tensorflow/tensorflow:latest-py3
