#!/bin/bash

if [[ "${1}" != "" ]] && [[ "${2}" != "" ]]
then
  if [[ "${3}" != "" ]]
  then
    ffmpeg -i "${1}" -vcodec libx265 -crf "${3}" "${2}"
    exit ${?}
  else
    ffmpeg -i "${1}" -vcodec libx265 -crf 24 "${2}"
    exit ${?}
  fi
else
  echo -e "Usage: ${0} <input_filename> <output_filename>\n (<constant_rate_factor:24>)"
  exit 0
fi
