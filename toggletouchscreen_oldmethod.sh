#!/bin/bash

tsid="$(xinput --list | sed -n '/Sharp Corp./{s/^.*id=//;s/\t.*$//;p}')"
if [[ "${tsid}" == "" ]]
then	echo "Touchscreen not detected." >&2
	exit 1
fi
#echo "Touchscreen detected" >&1
#echo "XID=${tsid}" >&1

enabled="$(xinput --list-props 10 | sed -n '/Device Enabled/{s/^.*\t//;p}')"
if [[ "${enabled}" == "" ]]
then	echo "Could not detect current state." >&2
	exit 2
fi
#if [[ "${enabled}" == "1" ]]
#then	wstate="enabled"
#else	wstate="disabled"
#fi
#echo "Currently ${wstate}, toggling..." >&1

if [[ "${enabled}" == "1" ]]
then	xinput disable ${tsid}
	notify-send -u normal -i emblem-system "Touchscreen disabled"
else	xinput enable ${tsid}
	notify-send -u normal -i emblem-system "Touchscreen enabled"
fi

exit 0
