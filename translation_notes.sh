#!/usr/bin/env bash

if [[ "${1}" != "" ]]
then  filename=${1}
else  echo -ne "Usage: ${0} <filename.html>\n\n"
      exit 1
fi

cp "${filename}" "${filename}.bk"

function selectline() {
  echo -ne "goto line (in html file, check with external editor) : "
  read currentline
  if [[ "${currentline}" == "" ]]
  then  echo -n "line number can't be left empty!\n"
        exit 1
  fi
}

selectline

function getindentation(){
  indentation="$(cat "${filename}" | sed -n "${currentline}p" | sed 's/^.*\(^[[:blank:]]*\).*$/\1/')"
  if [[ "$(cat "${filename}" | sed -n "$((${currentline}-1))p" | sed -n '/<p>/p')" != "" ]]
  then  preindentation="$(cat "${filename}" | sed -n "$((${currentline}-1))p" | sed 's/^.*\(^[[:blank:]]*\).*$/\1/')"
  else  preindentation="$(echo "${indentation}" | sed 's/\ \ $//')"
  fi
}

function addnewline() {
  getindentation
  #add newline (paragraph)
  cat "${filename}" | sed -n "1,${currentline}p" > "${filename}.edit"
  echo "${preindentation}</p>" >> "${filename}.edit" #currentline+1
  echo "${preindentation}<p>" >> "${filename}.edit"  #currentline+2
  echo "${indentation}" >> "${filename}.edit"  #currentline+3
  cat "${filename}" | sed -n "$((${currentline}+1)),\$p" >> "${filename}.edit"
  currentline=$((${currentline}+3))
  cp "${filename}.edit" "${filename}"
}

function addtag() {
  getindentation
  #tag (h1, h2, h3 or u) is given as an argument
  #write the content of the file up to cursor
  cat "${filename}" | sed -n "1,${currentline}p" > "${filename}.edit"
  #add a tag here
  echo "${indentation}<${1}>" >> "${filename}.edit"  #currentline+1
  echo "${indentation}  " >> "${filename}.edit"  #currentline+2
  echo "${indentation}</${1}>" >> "${filename}.edit" #currentline+3
  #write the rest of the file for update
  cat "${filename}" | sed -n "$((${currentline}+1)),\$p" >> "${filename}.edit"
  #update the line number
  currentline=$((${currentline}+2))
  #commit changes
  cp "${filename}.edit" "${filename}"
}

function getoutoftag() {
  getindentation
  #write the content of the file up the closure of the tag (currentline+1)
  cat "${filename}" | sed -n "1,$((${currentline}+1))p" > "${filename}.edit"
  #add an empty newline with preindentation
  echo "${preindentation}" >> "${filename}.edit"  #currentline+2
  #write the rest of the file for update
  cat "${filename}" | sed -n "$((${currentline}+2)),\$p" >> "${filename}.edit"
  #update the line number
  currentline=$((${currentline}+2))
  #commit changes
  cp "${filename}.edit" "${filename}"
}

function addtitle() {
  getindentation
  #tag (h1, h2, h3) is given as an argument
  #write the content of the file up to cursor
  cat "${filename}" | sed -n "1,${currentline}p" > "${filename}.edit"
  #close current paragraph (leaves the actual closure without an opening)
  echo "${preindentation}</p>" >> "${filename}.edit" #currentline+1
  #reopen a paragraph (no need to close it since a closure is already on the following line)
  echo "${preindentation}<p>" >> "${filename}.edit"  #currentline+2
  #prepare for writing in the new paragraph
  echo "${indentation}" >> "${filename}.edit"  #currentline+3
  #write the rest of the file for update
  cat "${filename}" | sed -n "$((${currentline}+1)),\$p" >> "${filename}.edit"
  #commit changes
  cp "${filename}.edit" "${filename}"
  #update line number
  currentline=$((${currentline}+1))
  #add a title here
  addtag "${1}"
}

function getoutoftitle() {
  currentline=$((${currentline}+3))
}


while true
do
  echo "###############################################"
  echo -ne "add word (leave empty for other options): "
  read newword
  if [[ "${newword}" != "" ]]
  then  echo -ne "word reading: "
        read reading
        echo -ne "short definition: "
        read definition
        if [[ "${reading}" != "" ]]
        then  if [[ "${definition}" != "" ]]
              then  #reading AND definition have been given
                    edittext="\\<span class=\\\"word\\\" title=\\\"${reading}: ${definition}\\\"\\>${newword}\\<\\/span\\>"
              else  #reading has been given, but not definition
                    edittext="\\<span class=\\\"word\\\" title=\\\"${reading}\\\"\\>${newword}\\<\\/span\\>"
              fi
        else  if [[ "${definition}" != "" ]]
              then  #definition has been given, but not reading
                    edittext="\\<span class=\\\"word\\\" title=\\\"${definition}\\\"\\>${newword}\\<\\/span\\>"
              else  #neither reading nor definition have been given
                    edittext="${newword}"
              fi
        fi

        cat "${filename}" | sed -n "1,$((${currentline}-1))p" > "${filename}.edit"
        cat "${filename}" | sed -n "${currentline}p" | sed "s/\$/${edittext}/" >> "${filename}.edit"
        cat "${filename}" | sed -n "$((${currentline}+1)),\$p" >> "${filename}.edit"

        cp "${filename}.edit" "${filename}"
  else  #no new word have been specified, printing menu options
        echo "___ < MENU > ___"
        echo -e "\tnew line: <empty>"
        echo -e "\tgoto line: g"
        echo -e "\tgoto line + new line: gg"
        echo -e "\tnew title 1: t"
        echo -e "\tnew title 2: tt"
        echo -e "\tnew title 3: ttt"
        echo -e "\tend of title: T"
        echo -e "\tstart tag (u, i, b): u || i || b"
        echo -e "\tend tag: uu || ii || bb"
        echo -e "\tcancel changes: c"
        echo -e "\texit: q"
        echo -ne "selection: "
        read menuselection

        getindentation

        case ${menuselection} in
              g)
                    #goto line
                    selectline
                    ;;
              gg)
                    #goto line + add new line
                    selectline
                    addnewline
                    ;;
              t)
                    #new title 1
                    addtitle h1
                    ;;
              tt)
                    #new title 2
                    addtitle h2
                    ;;
              ttt)
                    #new title 3
                    addtitle h3
                    ;;
              T)
                    getoutoftitle
                    ;;
              u)
                    #start underline
                    addtag u
                    ;;
              uu)
                    #end underline
                    getoutoftag
                    ;;
              i)
                    #start underline
                    addtag i
                    ;;
              ii)
                    #end underline
                    getoutoftag
                    ;;
              b)
                    #start underline
                    addtag b
                    ;;
              bb)
                    #end underline
                    getoutoftag
                    ;;
              c)
                    cp "${filename}.bk" "${filename}"
                    ;;
              q)
                    #exit
                    echo "quitting"
                    exit 0
                    ;;
              *)
                    #add newline (paragraph)
                    addnewline
                    ;;
        esac
  fi

done
