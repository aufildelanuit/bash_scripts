#!/bin/bash

# get vendor and product id
vendor_id="$(cat /proc/bus/input/devices | grep -B 1 "Sharp\ Corp." | sed -n "/Vendor/{s/^.*Vendor=//;s/\ .*$//;p}")"
product_id="$(cat /proc/bus/input/devices | grep -B 1 "Sharp\ Corp." | sed -n "/Product/{s/^.*Product=//;s/\ .*$//;p}")"

# make sure the first detection method worked...
# try over lsusb otherwise...
if [[ "${vendor_id}" == "" || "${product_id}" == "" ]]
then
    echo "Device not found in input devices list, trying from lsusb..."
    vendor_id="$(lsusb | grep "Sharp Corp." | sed "s/^.*ID //;s/\ .*$//;s/:.*$//")"
    product_id="$(lsusb | grep "Sharp Corp." | sed "s/^.*ID //;s/\ .*$//;s/^.*://")"
fi

# if again the device couldn't be found, abort...
if [[ "${vendor_id}" == "" || "${product_id}" == "" ]]
then
    echo "Device not found in lsusb list either, aborting..."
    exit 1
fi

# if the rule doesn't exist yet, create it (touchscreen will be disabled upon reload)
if [[ ! -e /etc/udev/rules.d/80-touchscreen.rules ]]
then
    echo "Disabling touchscreen..."
    action="disabled"
    echo "SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"${vendor_id}\", ATTRS{idProduct}==\"${product_id}\", ATTR{authorized}=\"0\"" | sudo tee /etc/udev/rules.d/80-touchscreen.rules
else
    # otherwise, get the current state
    current_state="$(cat /etc/udev/rules.d/80-touchscreen.rules | sed -n "/authorized/{s/^.*ATTR{authorized}=\"//;s/\"$//;p}")"
    # and try to invert it
    if [[ "${current_state}" == "0" ]]
    then
        echo "Enabling touchscreen..."
        action="enabled"
        new_state="1"
    elif [[ "${current_state}" == "1" ]]
    then
        echo "Disabling touchscreen..."
        action="disabled"
        new_state="0"
    # disable by default
    else
        echo "Disabling touchscreen..."
        action="disabled"
        new_state="0"
    fi
    echo "SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"${vendor_id}\", ATTRS{idProduct}==\"${product_id}\", ATTR{authorized}=\"${new_state}\"" | sudo tee /etc/udev/rules.d/80-touchscreen.rules
fi

# reload the rules
echo "reloading udev rules..."
sudo udevadm control --reload-rules && sudo udevadm trigger
notify-send "Touchscreen ${action}"
