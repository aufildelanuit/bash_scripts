#!/bin/bash

ssh labdesktop 'nc -lp 12800 ; \
                location="$(nc -lp 12801)"; \
                sudo umount Public/ ;  \
                sudo mount -t nfs "10.153.3.2:${location}" Public/ ; \
                if [[ $? == 0 ]]; then notify-send "${location} mounted in Public/ "; else notify-send "error while mounting ${location}"; fi' &

echo
until nc labdesktop 12800 -z
do    echo -ne "\rwaiting for remote availability"
      env sleep 1
done

echo -ne "\rok                                  \n"
cat /etc/exports | sed '/^[[:space:]]*$/d' | tail -n 1 | sed 's/\ .*$//;s/^\"//;s/\"[[:space:]]*$//' | nc labdesktop 12801 -N

exit $?
