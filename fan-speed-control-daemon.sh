#!/bin/bash

# This program needs to be run as root
if [[ $EUID -ne 0 ]]
then	echo "This program needs to be run as root (and not inside a container)..."
	sudo ${0} ${@}
	exit 0
fi

# Note: this program requires lm_sensors
temperature_sensor="coretemp-isa-0000"

echo "1" | tee /sys/devices/platform/asus-nb-wmi/hwmon/hwmon3/pwm1_enable

num_cores="$(sensors "${temperature_sensor}" -u | sed -n '/temp._input/{p}' | wc -l)"
first_core="$(sensors "${temperature_sensor}" -u | sed -n '/temp._input/{p}' | sed -n '1{s/^.*temp//;s/_.*$//;p}')"
last_core="$(sensors "${temperature_sensor}" -u | sed -n '/temp._input/{p}' | sed -n "${num_cores}{s/^.*temp//;s/_.*$//;p}")"

echo "Found ${num_cores} temperatures, listed from ${first_core} to ${last_core}."

function get_temp() {
	max_temp_ratio=0
	for core_i in $(seq ${first_core} ${last_core})
	do	current_temp_i="$(sensors "${temperature_sensor}" -u | sed -n "/temp${core_i}_input/p" | sed 's/^.*\:\ //' )"
		max_temp_i="$(sensors "${temperature_sensor}" -u | sed -n "/temp${core_i}_max/p" | sed 's/^.*\:\ //' )"
		temp_ratio_i="$(echo "scale=2; ${current_temp_i}/${max_temp_i}*100" | bc -l)"
		if (( $(echo "${temp_ratio_i} >= ${max_temp_ratio}" | bc -l) ))
		then	max_temp_ratio=${temp_ratio_i}
		fi
		echo "core ${core_i}: ${current_temp_i} / ${max_temp_i}  (${temp_ratio_i}%)"
	done
}

function set_fan_speed() {
	speed_param=85
	get_temp
	echo "Max temperature ratio: ${max_temp_ratio}%"
	if (( $(echo "${max_temp_ratio} <= 60.00" | bc -l) ))
	then	speed_param=85
	elif (( $(echo "${max_temp_ratio} < 85.00" | bc -l) ))
	then	speed_param=$(echo "(6.8 * ${max_temp_ratio} - 323)/1" | bc )
	else	speed_param=255
	fi
	echo "Selected speed parameter: ${speed_param}"
	echo "Writing to file..."
	echo ${speed_param} | tee /sys/devices/platform/asus-nb-wmi/hwmon/hwmon3/pwm1
}

function clean_and_stop() {
	echo -e "\n\nKeyboard Interrupt detected."
        sleep 1
        echo -e "\nCleaning up..."
        echo "2" | tee /sys/devices/platform/asus-nb-wmi/hwmon/hwmon3/pwm1_enable
	exit 0
}

trap clean_and_stop SIGINT
trap clean_and_stop SIGTSTP

echo ""
echo "This script will run forever and set fan speed every 10s."
echo "Please press Ctrl+C to stop it."
echo ""

while true
do	set_fan_speed
	env sleep 10
	echo ""
done

