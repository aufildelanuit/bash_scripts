#!/bin/bash

declare -A available_arguments
available_arguments[-o]=output_filename
available_arguments[-c]=compression_level
available_arguments[--compression-level]=compression_level
available_arguments[-h]=help
available_arguments[--help]=help

inp_fn=""
outp_fn=""
comp_word="prepress"
comp_lv="1"

function output_filename() {
	outp_fn="${1}"
	increment=2
}

function compression_level() {
	comp_lv="$(echo "${1}" | sed 's/[^0-9]*//g')"
	case "${comp_lv}" in
		1)
			comp_word="prepress"
			;;
		2)
			comp_word="ebook"
			;;
		3)
			comp_word="screen"
			;;
		*)
			comp_word=""
			;;
	esac
	increment=2
}

function help() {
	echo "Usage: ${0} [<options> <arguments>..] <input_filename.pdf>"
	echo ""
	echo "This program uses ghostscript to compress a pdf file."
	echo "Available options:"
	echo -e "\t-o\t\b\b\b\b--> output filename"
	echo -e "\t  \t  by default, append \"_compressed\" to the input filename"
	echo -e "\t  \t  \$ ${0} -o <output_filename> <input_filename>"
	echo -e "\t-c\t\b\b\b\b--> compression level, from 0 to 3"
	echo -e "\t  \t  0: same as original"
	echo -e "\t  \t  1: prepress / 300 dpi (default)"
	echo -e "\t  \t  2: ebook    / 150 dpi          "
	echo -e "\t  \t  3: screen   /  72 dpi          "
	echo -e "\t  \t  \$ ${0} -c <0..3> <input_filename>"
	echo -e "\t  \t  # equivalent to: --compression-level"
	echo -e "\t-h\t\b\b\b\b--> display this help message"
	echo -e "\t  \t  # equivalent to: --help"
	exit
}

function pdf_compress() {
	# check if the number of arguments is less than one
	if [[ ${#} -lt 1 ]]
	then
		echo "At least one argument is required : <input_filename.pdf>"
		echo "please refer to the following help message:"
		echo ""
		help
	fi
	# from here we suppose there is at least one argument
	i=1
	increment=1
	while [[ ${i} -le ${#} ]]
	do
		argument="$(eval "echo \$${i}")"
		if [[ "$(echo "${argument}" | cut -c 1)" == "-" ]]
		then
			if [[ ${available_arguments[${argument}]+_} ]]
			then
				eval "${available_arguments[${argument}]} ${@:$((${i}+1))}"
			else
				echo "Argument \"${argument}\" was not recognized."
				echo ""
				help
			fi
		else
			if [[ ${i} -eq ${#} ]]
			then
				inp_fn="${argument}"
			fi
		fi
		# DO NOT FORGET to increment the counter
		i=$((${i}+${increment}))
		increment=1
	done
	if [[ "${inp_fn}" == "" ]]
	then
		echo "A filename must be provided as the last argument."
		echo "please refer to the following help message:"
		echo ""
		help
	else
		# main command
		if [[ "${outp_fn}" == "" ]]
		then
			basename="$(echo "${inp_fn}" | sed 's/\....$//;s/\.....$//')"
			ext="$(echo "${inp_fn}" | sed 's/^.*\.//')"
			if [[ "${ext}" == "" || "$(echo "${inp_fn}" | sed -n '/\./p')" == "" ]]
			then
				outp_fn="${basename}_compressed"
			else
				outp_fn="${basename}_compressed.${ext}"
			fi
		fi
		if [[ "${comp_word}" == "" ]]
		then
			eval "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=\"${outp_fn}\" \"${inp_fn}\""
		else
			eval "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/${comp_word} -dNOPAUSE -dQUIET -dBATCH -sOutputFile=\"${outp_fn}\" \"${inp_fn}\""
		fi
	fi
}

pdf_compress "${@}"

