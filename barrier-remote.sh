#!/bin/bash

ssh labdesktop 'nohup bash -c "export DISPLAY=:0.0 && nohup barrier &>/dev/null &" &>/dev/null &'
if [[ ${?} == 0 ]]
then notify-send -u normal -i emblem-system "Remote Barrier activated"
     ssh labdesktop "notify-send -u normal -i emblem-system \"Barrier activated\""
     result=0
else notify-send -u normal -i emblem-system "Error while trying to remotely activate Barrier"
     result=1
fi

exit ${result}
