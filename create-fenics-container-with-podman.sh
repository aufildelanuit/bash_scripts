#!/bin/bash

#podman run -it --name fenics --pod new:fenics_pod -p 127.0.0.1:8000:8000 -v /var/home/${USER}/Programming/python/fenics/:/home/fenics/shared:z -w /home/fenics/shared --security-opt label=disable quay.io/fenicsproject/stable:current
podman run -it --name fenics --pod new:fenics_pod -p 127.0.0.1:8000:8000 -v /var/home/${USER}/Programming/python/fenics/:/home/fenics/shared:z -w /home/fenics/shared/Research --userns keep-id quay.io/fenicsproject/stable:current
