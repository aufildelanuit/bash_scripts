#!/bin/bash

# check if a tmp file exists, assume default parameters otherwise
# this trick allows keeping the parameter alive across executions
# if the program is called directly and not sourced.

TODO_FILE="${HOME}/.todo_list"


declare -A available_actions
available_actions[add]=todo_add
available_actions[append]=todo_append
available_actions[clear-all]=todo_clear_all
available_actions[delete]=todo_delete
available_actions[done]=todo_done
available_actions[edit]=todo_edit
available_actions[help]=todo_help
available_actions[list]=todo_list
available_actions[renum]=todo_renum

# Don't forget to update the help message
# if a function is added or modified


function todo_add() {
	list_size="$(cat "${TODO_FILE}" | wc -l)"
	# make sure the first argument is a line number
	# [^0-9]* means one or more characters that are not between 0 and 9, 'g' means all
	# we substitute all non-numerical characters by nothing, i.e., we remove them
	line_number="$(echo "${1}" | sed 's/[^0-9]*//g')"
	if [[ "${line_number}" != "" ]]
	then
		task_description="$(echo "${@:2}")"
		# if line_number is greater than (-gt) the list_size, append to the end of the file
		if [[ ${line_number} -gt ${list_size} ]]
		then
			echo "Provided line number larger than list size, appending..."
			eval "todo_append ${task_description}"
		else
			# i stands for 'insert'
			echo "$(cat "${TODO_FILE}" | sed "${line_number} i ${task_description}")" > "${TODO_FILE}"
			echo "The task \"${task_description}\" was added in position ${line_number}."
		fi
	else
		echo "Line number not provided, appending..."
		eval "todo_append ${@}"
	fi
}

function todo_append() {
	list_size="$(cat "${TODO_FILE}" | wc -l)"
	# append whatever was given in argument at the end of the todo list
	echo "${@}" >> "${TODO_FILE}"
	echo "The task \"${@}\" was added in position $(( ${list_size} + 1 ))."
}

function todo_clear_all() {
	read -e -n 1 -p "Are you really sure you want to clear all your tasks? [y/n] : " answer
	if [[ "${answer}" == "y" ]]
	then
		# delete all lines in the file
		sed -i d "${TODO_FILE}"
		echo "All tasks have been deleted. Time to try something new!"
	else
		echo "Aborting..."
	fi
}

function todo_delete() {
	line_number="$(echo "${1}" | sed 's/[^0-9]*//g')"
	list_size="$(cat "${TODO_FILE}" | wc -l)"
	if [[ "${line_number}" != "" ]]
	then
		# if line number is less or equal to list size...
		if [[ ${line_number} -le ${list_size} ]]
		then
			# get the task description by only printing the line we want
			task_description="$(cat "${TODO_FILE}" | sed -n "${line_number} p")"
			# delete the line (rewrite the file without it)
			echo "$(cat "${TODO_FILE}" | sed "${line_number} d")" > "${TODO_FILE}"
			echo "Task ${line_number} (${task_description}) was deleted, well done!"
		else
			echo "The provided task number doesn't exist, try again?"
		fi
	else
		echo "A task number must be provided. You can do it!"
	fi
}

function todo_done() {
	line_number="$(echo "${1}" | sed 's/[^0-9]*//g')"
	list_size="$(cat "${TODO_FILE}" | wc -l)"
	if [[ "${line_number}" != "" ]]
	then
		# if line number is less or equal to list size...
		if [[ ${line_number} -le ${list_size} ]]
		then
			# get the task description by only printing the line we want
			task_description="$(cat "${TODO_FILE}" | sed -n "${line_number} p")"
			# delete the line (rewrite the file without it)
			echo "$(cat "${TODO_FILE}" | sed "${line_number} d")" > "${TODO_FILE}"
			echo "Task ${line_number} (${task_description}) was marked as done, congratulations!"
		else
			echo "The provided task number doesn't exist, try again?"
		fi
	else
		echo "A task number must be provided. You can do it!"
	fi
}

function todo_edit() {
	line_number="$(echo "${1}" | sed 's/[^0-9]*//g')"
	list_size="$(cat "${TODO_FILE}" | wc -l)"
	if [[ "${line_number}" != "" ]]
	then
		# if line number is less or equal to list size...
		if [[ ${line_number} -le ${list_size} ]]
		then
			# get the task description by only printing the line we want
			task_description="$(cat "${TODO_FILE}" | sed -n "${line_number} p")"
			# now, allow the user to edit the task description
			read -e -p "Edit:  " -i "${task_description}" new_task_description
			# rewrite the file with a substitution on the desired line
			echo "$(cat "${TODO_FILE}" | sed "/${task_description}/{s/${task_description}/${new_task_description}/}")" > "${TODO_FILE}"
			echo "Task ${line_number} (${new_task_description}) was updated."
		else
			echo "The provided task number doesn't exist, try again?"
		fi
	else
		echo "A task number must be provided. You can do it!"
	fi
}

# display a help message on how to use this program
function todo_help() {
	# "-e" after "echo" allows the use of special characters, like 'tab' (\t)
	echo "Usage: todo <command> [<arguments>..]"
	echo ""
	echo "Short program to help you keep and manage a todo list."
	echo "(This program might require bash >= v4.0 to work properly)."
	echo ""
	echo "Available commands:"
	echo -e "\tadd      \t\b\b\b\b--> add an item to the todo list, at the given position."
	echo -e "\t         \t  \$ todo add <task_num> <task_description>"
	echo -e "\tappend   \t\b\b\b\b--> append an item at the end of the todo list."
	echo -e "\t         \t  \$ todo append <task_description>"
	echo -e "\tclear-all\t\b\b\b\b--> clear the entire todo list"
	echo -e "\t         \t(in case you would want a fresh start...)."
	echo -e "\t         \t  \$ todo clear-all"
	echo -e "\tdelete   \t\b\b\b\b--> remove a task from the list, from its number."
	echo -e "\t         \t  \$ todo delete <task_num>"
	echo -e "\tdone     \t\b\b\b\b--> mark a task as done (and delete it)."
	echo -e "\t         \t  \$ todo done <task_num>"
	echo -e "\tedit     \t\b\b\b\b--> edit a task on the todo list."
	echo -e "\t         \t  \$ todo edit <task_num>"
	echo -e "\thelp     \t\b\b\b\b--> display this help message"
	echo -e "\t         \t(also displayed if a command is not recognized)."
	echo -e "\t         \t  \$ todo help"
	echo -e "\tlist     \t\b\b\b\b--> list all the tasks in your todo list"
	echo -e "\t         \tif a filename is given, implicitely read from it"
	echo -e "\t         \t(called by default if no command is given)."
	echo -e "\t         \t  \$ todo [list]"
	echo -e "\trenum    \t\b\b\b\b--> change the position of a task in the todo list."
	echo -e "\t         \t  \$ todo renum <task_num> <new_task_num>"
}

function todo_list() {
	tasklist="$(cat "${TODO_FILE}")"
	if [[ "${tasklist}" != "" ]]
	then
		nl <(echo "${tasklist}")
	else
		echo "You don't have any task in your ToDo list for now. (Freedooom!)"
	fi
}

function todo_renum() {
	line_number="$(echo "${1}" | sed 's/[^0-9]*//g')"
	new_line_number="$(echo "${2}" | sed 's/[^0-9]*//g')"
	list_size="$(cat "${TODO_FILE}" | wc -l)"
	if [[ "${line_number}" != "" && "${new_line_number}" != "" ]]
	then
		# if line number is less or equal to list size...
		if [[ ${line_number} -le ${list_size} ]]
		then
			# get the task description by only printing the line we want
			task_description="$(cat "${TODO_FILE}" | sed -n "${line_number} p")"
			# remove the task at its current position
			echo "$(cat "${TODO_FILE}" | sed "${line_number} d")" > "${TODO_FILE}"
			# if the new position exists, insert the task there. Append at the end otherwise.
			if [[ ${new_line_number} -le ${list_size} ]]
			then
				# insert the task at the desired position
				echo "$(cat "${TODO_FILE}" | sed "${new_line_number} i ${task_description}")" > "${TODO_FILE}"
				echo "Task ${line_number} (${task_description}) was moved to position ${new_line_number}."
			else
				# append at the end of the file
				echo "${task_description}" >> "${TODO_FILE}"
				echo "The task \"${task_description}\" was moved to position $(( ${list_size} + 1 ))."
			fi
		else
			echo "The provided task number doesn't exist, try again?"
		fi
	else
		echo "A task number and a new task number must be provided. You can do it!"
	fi
}


function todo() {
	# this function is the main function.
	# from here we should check the commands / arguments
	# and call the other task-specific functions.

	# make sure the file exists before we try to read it
	touch "${TODO_FILE}"

	# call the list function if no command is given (first argument empty)
	# the command is the first argument if this one is not empty
	if [[ "${1}" == "" ]]
	then
		command="list"
	else
		command="${1}"
	fi
	# check if the command is in the array of available_actions
	# call the associated function if the command is recognized
	# call the help function if the command is not recognized
	if [[ ${available_actions[${command}]+_} ]]
	then
		# launches the function associated with the command
		# pass all arguments (${@}) starting from the second one (:2) to the function
		# eval is needed because the function name is passed as a string...
		eval "${available_actions[${command}]} ${@:2}"
	else
		echo "Command \"${command}\" was not recognized."
		echo ""
		todo_help
	fi

	# if the command wasn't "list" or "help", display the task list after anyother command
	if [[ "${command}" != "list" && "${command}" != "help" && ${command} != "clear-all" ]]
	then
		echo ""
		eval "todo_list"
	fi
}

# if the file is called directly instead of being sourced
# launch the todo() function with all the arguments
todo "${@}"
